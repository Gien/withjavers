package withjavers.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.javers.core.diff.Change;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.shadow.Shadow;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.auditoria.withjavers.service.audit.IAuditService;
import com.auditoria.withjavers.web.rest.AuditController;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {
	
	@InjectMocks
	AuditController controller = new AuditController();
	
    @Mock
    private IAuditService javersService;
    
	private List<Change> changes = new ArrayList<Change>();
	private List<Shadow<Object>> shadows = new ArrayList<Shadow<Object>>();
	private List<CdoSnapshot> snapshots= new ArrayList<CdoSnapshot>();
	
	@Test
	public void getPersonChanges(){
		when(javersService.getPersonChanges(Mockito.anyString())).thenReturn(changes);
		assertEquals(HttpStatus.OK.value(), controller.getPersonChanges("").getStatusCode().value());
	}
	
	@Test
	public void getPersonShadows(){
		when(javersService.getPersonShadows()).thenReturn(shadows);
		assertEquals(HttpStatus.OK.value(), controller.getPersonShadows().getStatusCode().value());
	}
	
	@Test
	public void prueba(){
		when(javersService.getPersonSnapshots()).thenReturn(snapshots);
		assertEquals(HttpStatus.OK.value(), controller.getPersonSnapshots().getStatusCode().value());
	}

}
