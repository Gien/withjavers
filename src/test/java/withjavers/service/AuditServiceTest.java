package withjavers.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.javers.core.Javers;
import org.javers.core.diff.Change;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.repository.jql.JqlQuery;
import org.javers.repository.jql.QueryBuilder;
import org.javers.shadow.Shadow;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.auditoria.withjavers.service.audit.impl.AuditService;

@RunWith(MockitoJUnitRunner.class)
public class AuditServiceTest {
	
	@InjectMocks
	AuditService audit;
	
	@Mock
	Javers javers;
	
	@Mock
	QueryBuilder jqlQuery;
	
	private List<Change> changes = new ArrayList<Change>();
	
	private List<Shadow<Object>> shadows = new ArrayList<Shadow<Object>>();
	
	private List<CdoSnapshot> snapshots = new ArrayList<CdoSnapshot>();
	
	@Test//(expected = NullPointerException.class)
	public void getPersonChanges_exception(){
		when(javers.findChanges((JqlQuery) Mockito.any())).thenReturn(changes);
		try{
			assertThat(audit.getPersonChanges("")).isNotNull();
		}catch(Exception e){
			fail();
		}
	}
	
	@Test//(expected = NullPointerException.class)
	public void getgetPersonShadows_exception(){
		when(javers.findShadows((JqlQuery) Mockito.any())).thenReturn(shadows);
		try{
			assertThat(audit.getPersonShadows()).isNotNull();
		}catch(Exception e){
			fail();
		}
	}
	
	@Test//(expected = NullPointerException.class)
	public void getPersonSnapshots_exception(){
		when(javers.findSnapshots((JqlQuery) Mockito.any())).thenReturn(snapshots);
		try{
			assertThat(audit.getPersonSnapshots()).isNotNull();
		}catch(Exception e){
			fail();
		}
	}

}
