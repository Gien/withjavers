package com.auditoria.withjavers.persistence.repository;

import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.auditoria.withjavers.persistence.domain.Person;

@Repository
@JaversSpringDataAuditable
public interface IPersonRepository extends JpaRepository<Person, Integer> {
	
}
