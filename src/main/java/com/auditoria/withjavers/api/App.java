package com.auditoria.withjavers.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.auditoria.withjavers"})
@EntityScan("com.auditoria.withjavers.persistence.domain")
@EnableJpaRepositories("com.auditoria.withjavers.persistence.repository")
public class App
{
	//@SpringBootApplication(scanBasePackages = { "com.gonzalo.runH2App" })
    public static void main(String[] args)
    {
    	SpringApplication.run(App.class, args);
    }
}