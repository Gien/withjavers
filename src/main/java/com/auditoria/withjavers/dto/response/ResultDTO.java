package com.auditoria.withjavers.dto.response;

public class ResultDTO {
	private String result;

	public String getResult() {
		return result;
	}

	public ResultDTO setResult(String result) {
		this.result = result;
		return this;
	}
	
}
