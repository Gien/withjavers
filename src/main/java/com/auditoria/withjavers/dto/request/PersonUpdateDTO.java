package com.auditoria.withjavers.dto.request;

public class PersonUpdateDTO extends PersonDTO{

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
