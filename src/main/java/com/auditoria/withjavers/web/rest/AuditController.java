package com.auditoria.withjavers.web.rest;

import java.util.List;

import org.javers.core.diff.Change;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.shadow.Shadow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auditoria.withjavers.service.audit.IAuditService;
import com.auditoria.withjavers.web.api.IAuditControler;

@RestController
@RequestMapping(value = "/audit")
public class AuditController implements IAuditControler {

    @Autowired
    private IAuditService javersService;
    
    @Override
    public ResponseEntity<List<Change>> getPersonChanges(@PathVariable("id") String id) {
        return new ResponseEntity<List<Change>>(javersService.getPersonChanges(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Shadow<Object>>> getPersonShadows() {
        return new ResponseEntity<List<Shadow<Object>>>(javersService.getPersonShadows(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CdoSnapshot>> getPersonSnapshots() {
        return new ResponseEntity<List<CdoSnapshot>>(javersService.getPersonSnapshots(), HttpStatus.OK);
    }
}