package com.auditoria.withjavers.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auditoria.withjavers.dto.request.PersonDTO;
import com.auditoria.withjavers.dto.request.PersonUpdateDTO;
import com.auditoria.withjavers.dto.response.ResultDTO;
import com.auditoria.withjavers.service.person.IPersonService;

@Controller
@RequestMapping("/person")
public class PersonController {
	
	@Autowired
	IPersonService personService;
	
	@RequestMapping(value="/insert",
			method = RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResultDTO insertPerson(@RequestBody PersonDTO persona){
		return personService.insertPerson(persona);
	}
	
	@RequestMapping(value="/update",
			method = RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResultDTO updatePerson(@RequestBody PersonUpdateDTO persona){
		return personService.updatePerson(persona);
	}
	
}
