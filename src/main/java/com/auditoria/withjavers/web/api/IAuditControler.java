package com.auditoria.withjavers.web.api;

import java.util.List;

import org.javers.core.diff.Change;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.shadow.Shadow;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface IAuditControler {

    @RequestMapping(value="/changesPerson/{id}",
	        produces = { "application/json" },
	    	consumes = { "application/json" },
	        method = RequestMethod.POST)
	ResponseEntity<List<Change>> getPersonChanges(String id);

    @RequestMapping(value="/shadowsPerson",
	        produces = { "application/json" },
	        method = RequestMethod.GET)
	ResponseEntity<List<Shadow<Object>>> getPersonShadows();

    @RequestMapping(value="/snapshotsPerson",
	        produces = { "application/json" },
	        method = RequestMethod.GET)
	ResponseEntity<List<CdoSnapshot>> getPersonSnapshots();

}
