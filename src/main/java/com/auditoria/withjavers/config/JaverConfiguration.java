package com.auditoria.withjavers.config;

import java.util.Map;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.javers.spring.auditable.AuthorProvider;
import org.javers.spring.auditable.CommitPropertiesProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

@Configuration
public class JaverConfiguration {
	
	private static final String KEY = "usuarioBE"; 
	
	@Bean
    public CommitPropertiesProvider commitPropertiesProvider() {
        return new CommitPropertiesProvider() {
            @Override
            public Map<String, String> provide() {
                return ImmutableMap.of("Author", "Gonzalo");
            }
        };
    }

	/*** 
	 * Configuration the  AuthorProvider 
	 * @return 
	 */ 

//	@Bean  
//	public AuthorProvider authorProvider() {  
//		return new AuthorProvider() {
//			@Override
//			public String provide() {
//				Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
//				ObjectMapper oMapper = new ObjectMapper();
//				String author = oMapper.convertValue(auth.getDetails(), Map.class).get(KEY).toString();  
//		
//				if (author != null) { 
//					return author;
//				} else {
//					return "unknown";
//				}           
//			}
//		}; 
//	} 

// with spring security
//	@Bean
//    public AuthorProvider authorProvider() {
//        return new SpringSecurityAuthorProvider();
//    }
}
