package com.auditoria.withjavers.service.person.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.auditoria.withjavers.dto.request.PersonDTO;
import com.auditoria.withjavers.dto.request.PersonUpdateDTO;
import com.auditoria.withjavers.dto.response.ResultDTO;
import com.auditoria.withjavers.persistence.domain.Person;
import com.auditoria.withjavers.persistence.repository.IPersonRepository;
import com.auditoria.withjavers.service.person.IPersonService;

@Controller
@RequestMapping("/person")
public class PersonService implements IPersonService{
	
	@Autowired
	IPersonRepository repository;
	
	@Override
	public ResultDTO insertPerson(PersonDTO persona){
		repository.saveAndFlush(setPersonWithPersonaDTO(persona, new Person()));
		return new ResultDTO().setResult("OK");
	}

	private Person setPersonWithPersonaDTO(PersonDTO persona, Person person) {
		person.setDni(persona.getIdentificator());
		person.setName(persona.getName());
		person.setSurname(persona.getSurname());
		return person;
	}
	
	@Override
	public ResultDTO updatePerson(PersonUpdateDTO persona){
		repository.saveAndFlush(setPersonWithUpdateDTO(persona, repository.findOne(persona.getId())));
		return new ResultDTO().setResult("OK");
	}

	private Person setPersonWithUpdateDTO(PersonUpdateDTO persona, Person person) {
		person.setDni(persona.getIdentificator());
		person.setName(persona.getName());
		person.setSurname(persona.getSurname());
		return person;
	}
	
}
