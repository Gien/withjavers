package com.auditoria.withjavers.service.person;

import com.auditoria.withjavers.dto.request.PersonDTO;
import com.auditoria.withjavers.dto.request.PersonUpdateDTO;
import com.auditoria.withjavers.dto.response.ResultDTO;

public interface IPersonService {

	ResultDTO insertPerson(PersonDTO persona);

	ResultDTO updatePerson(PersonUpdateDTO persona);

}
