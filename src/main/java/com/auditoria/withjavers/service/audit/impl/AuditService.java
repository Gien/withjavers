package com.auditoria.withjavers.service.audit.impl;

import java.util.List;

import org.javers.core.Javers;
import org.javers.core.diff.Change;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.repository.jql.QueryBuilder;
import org.javers.shadow.Shadow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.auditoria.withjavers.persistence.domain.Person;
import com.auditoria.withjavers.service.audit.IAuditService;

@Component
public class AuditService implements IAuditService {
	
	@Autowired
    private Javers javers;
    
	@Override
    public List<Change> getPersonChanges(String id) {
        QueryBuilder jqlQuery = QueryBuilder.byInstanceId(id, Person.class);

        return javers.findChanges(jqlQuery.build());
    }
    
	@Override
    public List<Shadow<Object>> getPersonShadows() {
        QueryBuilder jqlQuery = QueryBuilder.byClass(Person.class);
        
        return javers.findShadows(jqlQuery.build());
    }

	@Override
    public List<CdoSnapshot> getPersonSnapshots() {
        QueryBuilder jqlQuery = QueryBuilder.byClass(Person.class);

        return javers.findSnapshots(jqlQuery.build());
    }

}
