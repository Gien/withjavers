package com.auditoria.withjavers.service.audit;

import java.util.List;

import org.javers.core.diff.Change;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.shadow.Shadow;

public interface IAuditService {

	List<Change> getPersonChanges(String id);

	List<Shadow<Object>> getPersonShadows();

	List<CdoSnapshot> getPersonSnapshots();

}
