- Javers

    - ¿Qué es?

		Es una herramienta de auditoria de repositorios auto configurable.

    - ¿Qué hace?

		Toma la configuración del repositorio que tengamos en properties, para crear su propio repositorio de custodia de datos, para poder desplegar un total de 4 tablas:

			· jv_global_id — domain object identifiers,

			· jv_commit — JaVers commits metadata,

			· jv_commit_property — commit properties,

			· jv_snapshot — domain object snapshots.

		jv_global_id: Almacena el id del objeto del repositorio sobre el cual estamos realizando el cambio. Es decir, en una tabla denominada usuario, donde su id fuese el campo identificador, guardaría exactamente el valor del identificador, para poder realizar las búsquedas a partir de este dato.

		jv_commit: Almacena los datos del commit producido asignándole un id único y la fecha en que se produjo el commit.

		jv_commit_property: Almacena las propiedades que se asignen al commit, estas variables pueden asignarse de forma dinámica, mediante la generación del @Bean.

		jv_snapshot: Almacena la información de cómo se encuentra el objeto en el momento del commit y a la vez las propiedades del objeto que han cambiado. Además lo relaciona con el jv_commit y con jv_global_id, para poder relacionar los cambios asignados a un objeto en concreto y en función de estos, asignar una versión y un tipo de commit(INITIAL, UPDATE, DELETE).

    - ¿Como lo hace?

		La herramienta va almacenando los cambios de los objetos mediante snapshot, se tratan de una visión de cómo se encuentra el objeto en el momento del cambio, y de que cambios se han aplicado.

		De tal forma, que cuando se consulte el id del objeto almacenado, pueda sacar una relación de los cambios sufridos por el objeto durante su vida.

---------------------------------------------------------------------------------------------------------

- Configuración

	Como hemos comentado la herramienta es auto configurable, por lo que tomara las propiedades del repositorio que nuestro aplicativo tenga para usarlo de forma propia.

	Lo primero indicar que trabaja con Spring Data JPA, pero aún no con Hibernate.

    - Maven dependencias de javers:

	<properties>
		<javers.version>3.7.7</javers.version>
	</properties>

	<dependencies>
		<!-- javers --> 
		<!-- https://mvnrepository.com/artifact/org.javers/javers-core -->
		<dependency>
		<groupId>org.javers</groupId>
		<artifactId>javers-core</artifactId>
		<version>${javers.version}</version>
		</dependency> 
		
		<!-- https://mvnrepository.com/artifact/org.javers/javers-spring-jpa -->
		<dependency>
		<groupId>org.javers</groupId>
		<artifactId>javers-spring-jpa</artifactId>
		<version>${javers.version}</version>
		</dependency> 
		
		<!-- https://mvnrepository.com/artifact/org.javers/javers-spring-boot-starter-sql -->
		<dependency>
		<groupId>org.javers</groupId>
		<artifactId>javers-spring-boot-starter-sql</artifactId>
		<version>${javers.version}</version>
		</dependency>
		<!-- end javers -->
	</dependencies>

    - Application properties de javers (javadoc - https://javers.org/javadoc_3.x/org/javers/core/JaversBuilder.html):

		Ejemplo application.yml

			javers:

				mappingStyle: FIELD

				algorithm: SIMPLE

				prettyPrint: true

				typeSafeValues: false

				newObjectSnapshot: false

				packagesToScan:

				auditableAspectEnabled: true

				springDataAuditableRepositoryAspectEnabled: true

		Ejemplo application.properties

			#Javers

			javers.mappingStyle=FIELD

			javers.algorithm=SIMPLE

			javers.prettyPrint=true

			javers.typeSafeValues=false

			javers.newObjectSnapshot=false

			javers.packagesToScan=

			javers.auditableAspectEnabled=true

			javers.springDataAuditableRepositoryAspectEnabled=true

    - JaversConfiguration.java:

		Para poder configurar javers y que se integre y registré el autor real del commit hay que generar una clase de configuración para que pueda cargar el @Bean.

			import java.util.Map;
			import org.javers.spring.auditable.AuthorProvider;
			import org.springframework.context.annotation.Bean;
			import org.springframework.context.annotation.Configuration;
			import org.springframework.security.core.Authentication;
			import org.springframework.security.core.context.SecurityContextHolder;
			import com.fasterxml.jackson.databind.ObjectMapper;

			/***
			*
			* javers Configuration
			*
			*/

			@Configuration
			public class JaverConfiguration {
				private static final String KEY = "usuarioBE";

				/***
				* Configuration the AuthorProvider (item author)
				* @return
				*/

				@Bean
				public AuthorProvider authorProvider() {

					return new AuthorProvider() {
						@Override
						public String provide() {
							String author = getAuthenticationSecurity().get(KEY).toString();

							if (author != null) {
								return author;
							} else {
								return "unknown";
							}
						}
					};
				}

				/***
				* Configuration commit poperties jv_commit_property in case need it
				* @return
				*/
			//	@Bean
			// 	public CommitPropertiesProvider commitPropertiesProvider() {
			// 		return new CommitPropertiesProvider() {
			// 			@Override
			// 			public Map<String, String> provide() {
			// 				return ImmutableMap.copyOf(getAuthenticationSecurity());
			// 			}
			// 		};
			// 	}

				@SuppressWarnings("unchecked")
				private Map<String, String> getAuthenticationSecurity() {
					Authentication auth = SecurityContextHolder.getContext().getAuthentication();
					ObjectMapper oMapper = new ObjectMapper();
					return oMapper.convertValue(auth.getDetails(), Map.class);
				}
			}

    - Repositorio Auto Configurado:

		Solo queda asignar las etiquetas a los repositorios que queramos realizarle al auditoria (@JaversSpringDataAuditable).

			import com.pbank.middleware.domain.Crm_params;
			import org.springframework.stereotype.Repository;
			import java.util.UUID;
			import org.javers.spring.annotation.JaversSpringDataAuditable;
			import org.springframework.data.jpa.repository.*;

			/**
			* Spring Data JPA repository for the Crm_params entity.
			*/
			@Repository
			@JaversSpringDataAuditable
			public interface Crm_paramsRepository extends JpaRepository<Crm_params, UUID> {

			}

---------------------------------------------------------------------------------------------------------

- Controller Audit con ejemplos (AuditApi.java y AuditController.java)

	- Api:

		public interface AuditApi {

			@ApiOperation(value = "Get Changes in params with id", notes = "", response = Void.class, tags={ "Add new parameters", })
			@ApiResponses(value = {
				@ApiResponse(code = 204, message = "Ok", response = Void.class),
				@ApiResponse(code = 400, message = "InvalidqueryParameter", response = ApiError.class),
				@ApiResponse(code = 403, message = "Forbidden", response = ApiError.class),
				@ApiResponse(code = 404, message = "ResourceNotFound", response = ApiError.class),
				@ApiResponse(code = 405, message = "Method not allowed", response = ApiError.class),
				@ApiResponse(code = 503, message = "Service Unavailable", response = ApiError.class)
			})

			@RequestMapping(value = "/auditChanges/{id}",
				produces = { "application/json" },
				consumes = { "application/json" },
				method = RequestMethod.POST)
			ResponseEntity<List<Change>> getParamChanges(String id);

			@ApiOperation(value = "Get shadows in params", notes = "", response = Void.class, tags={ "Add new parameters", })
			@ApiResponses(value = {
				@ApiResponse(code = 204, message = "Ok", response = Void.class),
				@ApiResponse(code = 400, message = "InvalidqueryParameter", response = ApiError.class),
				@ApiResponse(code = 403, message = "Forbidden", response = ApiError.class),
				@ApiResponse(code = 404, message = "ResourceNotFound", response = ApiError.class),
				@ApiResponse(code = 405, message = "Method not allowed", response = ApiError.class),
				@ApiResponse(code = 503, message = "Service Unavailable", response = ApiError.class)
			})

			@RequestMapping(value="/auditShadows",
				produces = { "application/json" },
				method = RequestMethod.GET)
			ResponseEntity<List<Shadow<Object>>> getParamShadows();

			@ApiOperation(value = "Get Snapshots in params", notes = "", response = Void.class, tags={ "Add new parameters", })
			@ApiResponses(value = {
				@ApiResponse(code = 204, message = "Ok", response = Void.class),
				@ApiResponse(code = 400, message = "InvalidqueryParameter", response = ApiError.class),
				@ApiResponse(code = 403, message = "Forbidden", response = ApiError.class),
				@ApiResponse(code = 404, message = "ResourceNotFound", response = ApiError.class),
				@ApiResponse(code = 405, message = "Method not allowed", response = ApiError.class),
				@ApiResponse(code = 503, message = "Service Unavailable", response = ApiError.class)
			})

			@RequestMapping(value="/auditSnapshots",
				produces = { "application/json" },
				method = RequestMethod.GET)
			ResponseEntity<AuditSnapshots> getParamSnapshots();

		}

	- Ejemplo para consultar los cambios de un Id concreto para el objeto Crm_params:

		@Override
		public List<Change> getParamChanges(String id) {
			QueryBuilder jqlQuery = QueryBuilder.byInstanceId(id, Crm_params.class);

			return javers.findChanges(jqlQuery.build());
		}

	- Ejemplo para consultar los Shadows, en este caso sin especificar que Id, para el objeto Crm_params:

		@Override
		public List<Shadow<Object>> getParamShadows() {
			QueryBuilder jqlQuery = QueryBuilder.byClass(Crm_params.class);

			return javers.findShadows(jqlQuery.build());
		}

	- Ejemplo para consultar los Snapshot, en este caso sin especificar que Id, para el objeto Crm_params y transformándolos en una salida concreta:

		@Override
		public AuditSnapshots getParamSnapshots() {
			AuditSnapshots snapshots = new AuditSnapshots();
			QueryBuilder jqlQuery = QueryBuilder.byClass(Crm_params.class);
			
			for (CdoSnapshot snapshot : javers.findSnapshots(jqlQuery.build())){
				AuditSnapshot auditSnapshot = new AuditSnapshot();
				auditSnapshot.id(""+snapshot.getCommitId())
					.typeCommit(snapshot.getType().name())
					.dateCommit(snapshot.getCommitMetadata()
					.getCommitDate().toString())
					.objectCommit(snapshot.getGlobalId()
					.getTypeName());
					
				snapshots.add(auditSnapshot);
			}
			return snapshots;
		}
---------------------------------------------------------------------------------------------------------
- Anexos: 
	Página oficial javers (https://javers.org/)
	Pagina configuración javers con spring.boot (https://javers.org/documentation/spring-boot-integration/)
	Ejemplos de consulta de javers. (https://javers.org/documentation/jql-examples/#query-types)